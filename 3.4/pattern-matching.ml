let author =
  ("Atsushi", "Igarashi", 174.0, 61.0);;

let big_tupple =
  ((3, 'a'), (9.3, "Hello", false));;

let (first_name, last_name, height, weight) = author;;
(* 一部の情報が必要なければGoと同じように_を使う *)
let (name, _, _, _) = author;;
let (x, y) = big_tupple;;

