let average p =
  let (x, y) = p in 
  (x +. y) /. 2.;;

let average2 (x, y) =
  (x +. y) /. 2.;;
