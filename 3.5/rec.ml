(* recをつけると=の右側も宣言する変数の有効範囲に入る*)
let rec fact n =
  if n = 1 then 1
  else
    n * fact (n-1);;

let rec fib n =
  if n = 1 then 1 else
  if n = 2 then 1
  else
    fib (n - 1) + fib (n - 2);;

let fib1 n =
  let rec fib_pair n =
    if n = 1 then (1, 0)
    else
      let (i, j) = fib_pair (n - 1) in 
      (i + j, i)
  in 
  let (i, _) = fib_pair n in 
  i;;

print_int (fib1 1000);;
print_endline "";;


let fact1 n =
  let rec iter_fact (i, res) =
    if i = n then res * i
    else
      iter_fact (i+1, i*res)
  in 
  iter_fact (1, 1);;

print_int (fact1 45);;
print_endline "";;

let fact2 n =
  let rec tailfact (i, res) =
    if i = 1 then res
    else
      tailfact (i-1, res*i)
  in 
  tailfact (n, 1);;

print_int (fact2 4);;
print_endline "";;