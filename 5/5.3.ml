let rec length = function
    [] -> 0
  | _ :: rest -> 1 + length rest;;

(*これはfunction構文を使わない（使えない？）*)
(*functionを使えるのは最後の引数に対して即座にパターンマッチングする場合のみっぽい？*)
(*appendは中置演算子@として組み込みで定義されている*)
let rec append l1 l2 =
  match l1 with
    [] -> l2
  | v :: l1' -> v :: (append l1' l2);;

let rec reverse = function
    [] -> []
  | v :: rest -> reverse rest @ [v];;

let rec revAppend l1 l2 = 
  match l1 with
    [] -> l2
  | v :: l1' -> revAppend l1' (v :: l2);;

let efficient_rev = function
    [] -> []
  | v :: rest -> revAppend rest [v];;

let rec map f = function
    [] -> []
  | x :: rest -> f x :: map f rest;;

let rec forall p = function
    [] -> true
  | v :: rest -> if p v then forall p rest else false;;

let rec my_exist p = function
    [] -> false
  | v :: rest -> if p v then true else my_exist p rest;;

let rec exist p = function
    [] -> false
  | x :: rest -> (p x) || exist p rest;;

let rec fold_right f l e =
  match l with
    [] -> e
  | x :: rest -> f x (fold_right f rest e);;
