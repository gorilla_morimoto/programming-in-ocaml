let rec nth1 n l =
  if n = 1 then
    let a :: _ = l in a
  else
    let _ :: rest = l in nth1 (n - 1) rest;;

let rec nth2 n l =
  match n with
    1 -> let a :: _ = l in a
  | _ -> let _ :: rest = l in nth2 (n - 1) rest;;

let rec nth3 n l =
  match (n, l) with
    (1, a :: _) -> a
  | (_, _ :: rest) -> nth3 (n - 1) rest;;

let rec nth4 n l =
  match (n, l) with
    (1, a :: _) -> a
  | (n', _ :: rest) when n' > 0 -> nth4 (n - 1) rest;;

let f = function
    [] -> 0
  | x :: rest when x > 0 -> 1
  | x :: rest when x <= 0 -> 2;;

let g = function
    [] -> 0
  | x :: rest when x > 0 -> 1
  | x :: rest -> 2;;

let rec nth5 n l =
  match n with
    1 -> (match l with a :: _ -> a)
  | n' when n' > 0 -> (match l with _ :: rest -> nth5 (n - 1) rest);;
