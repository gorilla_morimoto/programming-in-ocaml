(*let <name> [pattern] = <expression>*)
let sum_list3 [x; y; z] = x + y + z;;

let rec sum_list l =
  match l with
    [] -> 0
  | n :: rest -> n + (sum_list rest);;

(*function構文は最後の引数に対して即座にパターンマッチングを行う*)
(*引数をパターンマッチング以外に使わないときに便利*)
let rec new_sum_list = function
    [] -> 0
  | n :: rest -> n + (new_sum_list rest);;