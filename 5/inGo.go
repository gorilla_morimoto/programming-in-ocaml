package main

import "fmt"

func main() {
	l := []int{4, 3, 2}
	fmt.Println(sumList(l))
	fmt.Println(
		forall(
			func(x int) bool {
				if x >= 1 {
					return true
				}
				return false
			},
			l,
		),
	)

	// insertの確認
	l2 := []int{2, 4, 5}
	fmt.Println(insert(3, l2))
	// insertionSort
	l3 := []int{5, 33, 6, 3, 2, 57, 4}
	fmt.Println(insertionSort(l3))

}

func sumList(l []int) int {
	if len(l) == 0 {
		return 0
	}

	return l[0] + sumList(l[1:])
}

// 内部で同じ挙動をするlwn()を使ってしまっているので意味がなさそう
// func length(l []int) int {
// 	if len(l) == 0 {
// 		return 0
// 	}
// 	return 1 + length(l[1:])
// }

// コンスが使えないのでGoでは同じロジックでは実装できなさそう
// func newAppend(l1, l2 []int) []int {
// 	if len(l1) == 0 {
// 		return l2
// 	}
//
// 	return l1[0] :: newAppend(l1[1:], l2)

func forall(f func(int) bool, l []int) bool {
	if len(l) == 0 {
		return true
	}

	if !f(l[0]) {
		return false
	}

	return forall(f, l[1:])
}

func insert(x int, ls []int) []int {
	if len(ls) == 0 {
		ls = append(ls, x)
		return ls
	}

	y := ls[0]
	rest := ls[1:]

	if x < y {
		// splat展開するときは複数の要素を渡せないらしい
		// []int{}のcapは0なのでnewLsは新しい配列が割り当てられる）
		newLs := append([]int{}, x)
		// newLs := []int{x} と同じこと
		newLs = append(newLs, ls...)
		return newLs
	}

	newLs := []int{y}
	newLs = append(newLs, insert(x, rest)...)
	return newLs
}

func insertionSort(ls []int) []int {
	if len(ls) == 0 {
		return ls
	}

	x := ls[0]
	rest := ls[1:]

	return insert(x, insertionSort(rest))
}
