(*5.1*)
let week = ["Sun"; "Mon"; "Tue"; "Wed"; "Thu"; "Fri"; "Sat"];;
let empty_list = [];;
let oddnums = [3; 9; 253];;
(*コンス*)
let more_oddnums = 99 :: oddnums;;
(*コンスは右結合*)
let evennums = 4 :: 10 :: [256; 12];;
