let rec sum_of (f, n) =
  if n = 0 then 0
  else sum_of (f, n - 1) + (f n)
let sum_of_square n = sum_of ((fun x -> x * x), n);;
let sum_of_cube n = sum_of ((fun x -> x * x * x), n);;

(*let <name> [pattern] = <expression> は*)
(*let <name> = fun [pattern] -> <expression> のシンタックスシュガー*)