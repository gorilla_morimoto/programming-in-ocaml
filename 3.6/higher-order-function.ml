let rec sum_of_square n =
  if n = 0 then 0
  else (n * n) + (sum_of_square (n - 1));;

print_int (sum_of_square 4);;
print_endline "";;

let rec sum_of_cube n =
  if n = 0 then 0
  else (n * n * n) + (sum_of_cube (n - 1));;

print_int (sum_of_cube 4);;
print_endline "";;

let rec sum_of (f, n) =
  if n = 0 then 0
  else sum_of (f, n - 1) + f n;;

let square n = n * n;;
let new_sum_of_square n = sum_of (square, n);;
let cube n = n * n * n;;
let new_sum_of_cube n = sum_of (cube, n);;

print_int (new_sum_of_square 4);;
print_endline "";;