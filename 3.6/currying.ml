let concat_curry s1 = (fun s2 -> s1 ^ s2 ^ s1);;
let concat_curry1 s1 s2 = s1 ^ s2 ^ s1;;

let rec sum_of f n =
  if n = 0 then 0
  else f n + sum_of f (n-1);;