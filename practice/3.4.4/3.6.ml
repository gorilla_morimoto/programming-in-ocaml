let geo_mean (x, y) =
  (x *. y) ** 0.5;;

let bmi (name, height, weight) =
  let index = weight /. (height ** 2.) in 
  if index < 18.5 then name ^ "さんは痩せています" else
  if 18.5 <= index  && index < 25. then name ^ "さんは標準です" else
  if 25. <= index && index < 30. then name ^ "さんは肥満です" else
    name ^ "さんは高度肥満です";;

let sum_and_diff (x, y) = 
  (x + y, x - y);; 

(* 引数の型を指定する場合には以下のように記述する *)
(* 複数の引数の型を指定するなら*)
(* let f (x:int) (y:int) *)
let f (t : int*int) =
  let (x_plus_y, x_minus_y) = t in 
  let x = (x_plus_y + x_minus_y) / 2 in 
  let y = (x_plus_y - x_minus_y) / 2 in 
  (x, y);;

let g (x, y) =
  ((x + y) / 2, (x - y) / 2);;
