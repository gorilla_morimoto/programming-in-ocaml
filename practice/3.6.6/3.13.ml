let rec pow x n =
  if n = 0 then 1
  else x * pow x (n - 1);;

let rec pow2 n x =
  if n = 1 then x
  else
  if n mod 2 = 0 then pow2 (n / 2) (x * x)
  else pow2 ((n + 1) / 2) (x * x) / x;;

let cube = pow2 3;;

print_int (cube 3);;
print_endline;;
