let dollar_to_yen dollar =
  let rate = 114.32 in
  floor (dollar *. rate);;

Printf.printf ("%f\n") (dollar_to_yen 1.);;

(* 四捨五入を直接実行してくれる関数はないので定義します *)
let round x = floor (x +. 0.5);;
let yen_to_dollar yen =
  let rate = 114.32 in
  let fyen = float yen in 
  round (fyen /. rate);;

(*PrintfのフォーマットはGoとだいたい同じっぽい*)
Printf.printf ("%.2f\n") (yen_to_dollar 4000);;

let format_display dollar =
  let yen = dollar_to_yen dollar in 
  (string_of_float dollar) ^ " dollars are " ^ (string_of_float yen) ^ " yen.";;

Printf.printf ("%s\n") (format_display 100.);;

(* この問題はまったくわからなかった *)
(* charがASCIIコード？として解釈されることを利用するっぽい *)
(* https://github.com/kaznum/programming_in_ocaml_exercise/blob/master/ex3.1/capitalize.ml *)
let capitalize c = 
  if 'a' <= c && c <= 'z'
  then char_of_int(int_of_char(c) - 32)
  else c ;;

Printf.printf ("%c\n") (capitalize 'c');;