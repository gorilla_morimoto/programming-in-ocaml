let rec gcd (m, n) = 
  if m = n then m 
  else if m > n then gcd (m - n, n)
  else gcd (n - m, m);;

Printf.printf "%d\n" (gcd (42, 70));;
Printf.printf "%d\n" (gcd (70, 42));;

let rec comb (n, m) = 
  if n = m || m = 0 then 1
  else comb (n - 1, m) + comb (n - 1, m - 1);;

Printf.printf "%d\n" (comb (7, 2));;

let iterfib n = 
  let rec f (now, before, i) =
    if i = n then now
    else f (now + before, now, (i + 1))
  in 
  f (1, 0, 1);;

Printf.printf "%d\n" (iterfib 6);;